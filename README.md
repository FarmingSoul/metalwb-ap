# FreeCAD Metal Workbench

MetalWB is a workbench for creation of mechanical welding parts. In its final version, it should allow to cover the whole welding design workflow :
1) Skeleton creation with sketch lines or geometrical primitives;
2) Application of a profile on the skeleton, to create a part;
3) Adjustment functions (e.g. removal of overlaps) or addition of specific elements (gusset, weld bead, etc.);
4) Export of a spreadsheet that lists each part of the assembly, identifies the profiles and their length.
